import { CoffeeOrderPage } from './app.po';

describe('coffee-order App', () => {
  let page: CoffeeOrderPage;

  beforeEach(() => {
    page = new CoffeeOrderPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
