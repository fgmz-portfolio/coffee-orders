import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import * as _ from "lodash";

import { CoffeeOrder } from "../../models/order";

@Injectable()
export class CoffeeService {

	private retryTime = 120000;
	private checkTime = 10000;
	private taskCounter = 0;
	private coffeeStack = [
		'Death Wish Coffee',
		'Lungo',
		'Black Coffee',
		'Babycino',
		'Coffee With Cream and Sugar',
		'Affogato',
		'Americano',
		'Cortado',
		'Vietnamese Iced Coffee',
		'Cappuccino Scuro',
		'Espresso Con Panna',
		'Ristretto',
		'Short Black',
		'Con Panna',
		'Cappuccino Chiaro',
	];
	private orderSource = new Subject<any>(); 
	public orders = [];

  constructor() {

  	this.fetchFirstOrder();
  }

  fetchFirstOrder(){

  	this.getOrders()
  	.subscribe( order =>{
  		this.orders.push(order);
  	});
  	this.waitForNextOrder();

  }

  getOrders():Observable<any>{

  	return this.orderSource.asObservable();

  }

  getNextOrder():Promise<any>{

  	return new Promise((resolve, reject) => {

	  	let index = Math.floor((Math.random() * this.coffeeStack.length) + 0);

	  	let order = new CoffeeOrder( this.coffeeStack[index], this.taskCounter );

	  	this.orderSource.next(order);
	  	this.sortOrders();
	  	this.taskCounter++;

	  	
  		resolve(order);
  	});

  }

  waitForNextOrder() {

  	// stop at 15 orders
  	if(this.taskCounter < 15 ){

  		let time = Math.floor((Math.random() * 10) + 5) * 1000;
  		console.log('next order in: '+time);

  		setTimeout( _ => {

  			this.getNextOrder()
		  	.then(
		  		order => { 

		  			this.waitForStatus(order);

		  			this.waitForNextOrder();

		  		}
		  	);

  		}, time);	
  	} 
  	
  }

  waitForStatus(order, time?:number ){

  	if(!time){
  		time = this.checkTime;
  	}

  	// create timer and wait then CHECK STATUS for last ORDER 
  	// last order should be checked 
  	// have shifted position from the array 

  	// 10 seconds wait
  	let statusTimer = Observable.timer(time);
  	statusTimer.subscribe( 
  		_ => {
  			
  			
  			this.checkStatus()
  			.then(
  				// success
  				() => {
  					this.orderSuccess(order);
  					
  				},
  				// failure
  				() => {
  					this.orderFailed(order);
  					
  				}
  			);
  		}
  	);
  }

  checkStatus():Promise<any>{

  	return new Promise((resolve, reject) => {
  		let prob = Math.floor((Math.random() * 10) + 1);

  		if(prob <= 8 ){
  			// succeded
  			resolve();
  		}else{
  			// failed
  			reject();
  		}
  	});

  }

  orderSuccess(order){
			
		let index = this.findOrder(order);

		this.orders[index].status.label = "Done";
		this.orders[index].status.isPending = false;
		this.orders[index].status.isFailed = false;
		this.orders[index].status.isDone = true;

		this.sortOrders();
		
  }

  orderFailed(order){
		// find order in this.orders and change status to failure
		let index = this.findOrder(order);

		this.orders[index].status.label = "Error, retrying in 2 minutes";
		this.orders[index].status.isPending = false;
		this.orders[index].status.isDone = false;
		this.orders[index].status.isFailed = true;

		// then retry
		this.waitForStatus(order,this.retryTime);
		this.sortOrders();
	
  }

  findOrder(order){
  	// find order in this.orders and change status to success
		return _.findIndex(
			this.orders,
			function(o) { 
				return o.id == order.id; 
			}
		);
  }
  

  sortOrders(){
  	// then move it to the bottom

		this.orders.sort( (a,b) => {

			if (a.status.isPending === b.status.isPending) {
         if (a.status.isFailed === b.status.isFailed) {
             return (a.status.isDone > b.status.isDone) ? -1 : (a.status.isDone < b.status.isDone) ? 1 : 0;
         } else {
             return (a.status.isFailed > b.status.isFailed) ? -1 : 1;
         }
	    } else {
	         return (a.status.isPending > b.status.isPending) ? -1 : 1;
	    }

		});
  }

  remove(order){
  	let index = this.findOrder(order);
  	this.orders.splice(index,1);
  }

}
