import { Component, OnInit } from '@angular/core';

import { CoffeeService } from '../../providers/coffee-service/coffee.service';

@Component({
  selector: 'order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

	orders: any = [];

  constructor( public coffeeService: CoffeeService ) {
  	
  }

  ngOnInit() {
  	// bind coffeeService.orders to component's orders
  	this.orders = this.coffeeService.orders;
  }

  remove(item){
    this.coffeeService.remove(item);
  }

}
