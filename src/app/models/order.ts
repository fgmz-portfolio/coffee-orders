export class CoffeeOrder {
	public name:string = '';
	public id:number;
	public status =  {
		label: "Pending",
		isPending: true,
		isFailed: false,
		isDone: false
	};

	constructor(name:string, index){
		this.id = index+1;
		this.name = name;
	}
}